﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility : MonoBehaviour
{

    public static Vector3 mouseToWorldPosition()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;
        return mousePos;
    }
    public static void changeAlphaSprite(SpriteRenderer sr, float alpha = 0f)
    {
        Color c = sr.color;
        c.a = alpha;
        sr.color = c;
    }
    public static T[] ListToArray<T>(List<T> list)
    {
        T[] resultArray = new T[list.Count];
        for (int i = 0; i < resultArray.Length; i++)
        {
            resultArray[i] = list[i];
        }
        return resultArray;
    }
}
