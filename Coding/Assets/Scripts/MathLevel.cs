﻿using System.Collections;
using System.Collections.Generic;
using AlisCoding;
using UnityEngine;

public class MathLevel : MonoBehaviour 
{
    public MathProblem problem
    {
        get;
        set;
    }

    public List<Vertex> bestPath
    {
        get;
        set;
    }

    public int bestStepCount
    {
        get;
        set;
    }




    // Use this for initialization
	void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}
}
