﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class TileGroupController : MonoBehaviour 
{
    public Transform centerTile;

    public Transform[] allTiles;

    public static TileGroupController Instance;

    private void Awake()
    {
        Instance = this;
    }

	void Start () 
    {
        allTiles = new Transform[transform.childCount];
        for (int i = 0; i < allTiles.Length; i++)
        {
            allTiles[i] = transform.GetChild(i);
            Utility.changeAlphaSprite(allTiles[i].GetComponent<SpriteRenderer>());
        }
        openAllTiles();
        PlayerController.Instance.transform.position = centerTile.transform.position;
    }

    // Update is called once per frame
    void Update () 
    {
		
	}

    public void openAllTiles()
    {
        StartCoroutine(fadeOpenTilesTask());
    }

    private IEnumerator fadeOpenTilesTask()
    {
        for (int i = 0; i < allTiles.Length; i++)
        {
            allTiles[i].GetComponent<SpriteRenderer>().DOFade(1f, 0.05f);
            yield return new WaitForSeconds(0.05f);
        }
    }
}
