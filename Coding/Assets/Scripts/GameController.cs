﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameController : MonoBehaviour 
{
    public static GameController Instance;
    public enum Action { goRight, goLeft, goUp, goDown }

    public Transform actionCollectionPanel;
    public Transform goldImage;
    public Transform canvas;

    public AnimationCurve goldMoveCurve;

    private Transform holdActionItem = null;
    private bool _onPointerOverCollection = false;
    private int gold = 0;



    private List<Action> actionList;
    private void Awake()
    {
        Instance = this;
    }
    // Use this for initialization
    void Start () 
    {
        actionList = new List<Action>();

	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public List<Action> getActions()
    {
        return actionList;
    }

    public void onActionClick(ActionItem actionItem)
    {
        GameObject actionItemGO = Instantiate(actionItem.gameObject, actionCollectionPanel);
        actionItemGO.transform.DOKill();
        actionItemGO.GetComponent<ActionItem>().resetScale();
        float tempScale = actionItemGO.transform.localScale.x;
        actionItemGO.transform.localScale = Vector3.zero;
        actionItemGO.transform.DOScale(tempScale, 1f).SetEase(Ease.OutElastic,2f);
        actionList.Add(actionItem.action);

    }

    public void onActionBeginDrag(ActionItem actionItem)
    {
        GameObject actionItemGO = Instantiate(actionItem.gameObject,canvas);
        holdActionItem = actionItemGO.transform;
        holdActionItem.transform.DOKill();
        holdActionItem.GetComponent<ActionItem>().resetScale();
        float tempScale = holdActionItem.transform.localScale.x;
        holdActionItem.transform.localScale = Vector3.zero;
        holdActionItem.transform.DOScale(tempScale, 1f).SetEase(Ease.OutElastic, 2f);
        holdActionItem.GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void onActionDrag()
    {
        holdActionItem.transform.position = Utility.mouseToWorldPosition();

    }
    public void onActionEndDrag()
    {
        if(_onPointerOverCollection)
        {
            holdActionItem.transform.SetParent(actionCollectionPanel);
            holdActionItem.GetComponent<ActionItem>().resetScale();
            float tempScale = holdActionItem.transform.localScale.x;
            holdActionItem.transform.localScale = Vector3.zero;
            holdActionItem.GetComponent<EventTrigger>().enabled = false;
            holdActionItem.transform.DOKill();
            holdActionItem.transform.DOScale(tempScale, 1f).SetEase(Ease.OutElastic, 2f);
            actionList.Add(holdActionItem.GetComponent<ActionItem>().action);
            holdActionItem.GetComponent<CanvasGroup>().blocksRaycasts = true;
        }
        else
        {
            Destroy(holdActionItem.gameObject);
        }



    }

    public void onPointerEnterCollection()
    {
        _onPointerOverCollection = true;
    }

    public void onPointerExitCollection()
    {
        _onPointerOverCollection = false;
    }

    public void setGold(int g)
    {
        gold = g;
    }

    public void changeGold(int g)
    {
        gold += g;
    }

    public int getGold()
    {
        return gold;
    }

    public bool canCollectGoldFromGround()
    {
        return true;
    }

    public void collectedGoldGoesToUI(Transform collectedGold)
    {
        //Vector3[] path = CurveController.Instance.getPointsFromGoldCurve(collectedGold.position, goldImage.position);
        //collectedGold.DOPath(path, 1f, PathType.CatmullRom, PathMode.Full3D, 30, Color.yellow).SetEase(goldMoveCurve).OnComplete(() => 
        //{
        //    Destroy(collectedGold.gameObject);
        //});
        collectedGold.DOMove(goldImage.position, 1f).SetEase(Ease.InQuad).OnComplete(() => 
        {
            Destroy(collectedGold.gameObject);
        });

    }




}
