﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PlayerController : MonoBehaviour 
{
    public static PlayerController Instance;

    private void Awake()
    {
        Instance = this;
    }
    // Use this for initialization
    void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            playActions();
        }
	}


    public void playActions()
    {
        StartCoroutine(playActionsTask());
    }
    IEnumerator playActionsTask()
    {
        List<GameController.Action> actionList = GameController.Instance.getActions();
        for (int i = 0; i < actionList.Count; i++)
        {
            if(actionList[i] == GameController.Action.goDown)
            {
                goDown();
            }
            else if (actionList[i] == GameController.Action.goUp)
            {
                goUp();
            }
            else if (actionList[i] == GameController.Action.goLeft)
            {
                goLeft();
            }
            else if (actionList[i] == GameController.Action.goRight)
            {
                goRight();
            }
            yield return new WaitForSeconds(1f);
        }
    }

    private void goLeft()
    {
        transform.DOMoveX(-1f, 1f).SetRelative();
    }
    private void goRight()
    {
        transform.DOMoveX(1f, 1f).SetRelative();
    }
    private void goDown()
    {
        transform.DOMoveY(-1f, 1f).SetRelative();
    }
    private void goUp()
    {
        transform.DOMoveY(1f, 1f).SetRelative();
    }
}
