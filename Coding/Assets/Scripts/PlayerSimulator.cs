﻿using System.Collections;
using System.Collections.Generic;
using AlisCoding;
using UnityEngine;



    public class PlayerSimulator : MonoBehaviour
    {
        int currentDirection = 3;
        Vertex currentVertex;

        List<int> actions;

        public static PlayerSimulator Instance;

        private void Awake()
        {
            Instance = this;
            actions = new List<int>();
        }
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        void useMovement(Vertex next, int dir)
        {
            currentVertex = next;
            Debug.Log(currentVertex);   
            actions.Add(10 + dir);
        }

        void usePortal(Vertex vertex)
        {
            currentVertex = vertex;
            Debug.Log(currentVertex);
            actions.Add(9);
        }

        void useRotation(int dir)
        {
            if (Mathf.Abs(dir - currentDirection) == 2)
            {
                actions.Add(5);
                actions.Add(5);
                
            }
            else if (Mathf.Abs(dir - currentDirection) == 3)
            {
                actions.Add(dir - currentDirection > 0 ? 5 : 6);

            }
            else
            {
                actions.Add(dir - currentDirection > 0 ? 6 : 5);
            }
            currentDirection = dir;
        }

        public void calculateTravel(Vertex start, List<Vertex> path)
        {
            currentVertex = start.CloneOnlyVertex();
            for (int i = 0; i < path.Count; i++)
            {
                int nextDir = nextDirection(currentVertex, path[i]);
                int diff = difference(currentVertex, path[i], nextDir);
                if (diff == 2)
                {
                    if (currentDirection != nextDir)
                    {
                        useRotation(nextDir);
                    }
                    usePortal(path[i]);
                }
                else
                {
                    useMovement(path[i], nextDir);
                    currentDirection = nextDir;
                }
            }
            Debug.Log("Toplam : " + actions.Count);
            
        }

        int difference(Vertex start, Vertex next, int dir)
        {
            if (dir == 0)
            {
                return next.x - start.x;
            }
            else if (dir == 2)
            {
                return start.x - next.x;
            }
            else if (dir == 1)
            {
                return next.y - start.y;
            }
            else
            {
                return start.y - next.y;
            }
        }

        int nextDirection(Vertex start, Vertex next)
        {
            if (next.x > start.x)
            {
                return 0;
            }
            else if (next.x < start.x)
            {
                return 2;
            }
            else if (next.y > start.y)
            {
                return 1;
            }
            else
            {
                return 3;
            }
        }
    }
