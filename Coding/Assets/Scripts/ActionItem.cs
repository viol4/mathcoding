﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionItem : MonoBehaviour 
{
    public GameController.Action action;

    private Vector3 startScale;

    private void Awake()
    {
        startScale = transform.localScale;
    }
    // Use this for initialization
    void Start () 
    {
        
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public void resetScale()
    {
        transform.localScale = startScale;
    }
}
