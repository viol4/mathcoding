﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

namespace AlisCoding
{
    public class CodingSimulator : MonoBehaviour
    {

       
        int[,] grid = new int[7, 7];
        List<Vertex> vertices;
        MathProblem currentProblem;
        // Use this for initialization
        void Start()
        {
            
            //GetShortestPath(0,0,4,4);


        }

        // Update is called once per frame
        void Update()
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                MathProblem problem = MathController.Instance.generateMathProblemEasy(3);
                MathController.Instance.fixProblem(problem);
                generatePlatform(7, 7, problem);

                createObjectsOnMap();
            }
        }

        void prepareEdges(List<Vertex> _vertices)
        {
            




            for (int i = 0; i < _vertices.Count(); i++)
            {
                List<Vertex> neighbours = neightborVerticesRaw(_vertices, _vertices[i]);
                for (int k = 0; k < neighbours.Count; k++)
                {
                    if (isNormalTile(neighbours[k].x, neighbours[k].y))
                    {
                        _vertices[i].addEdge(neighbours[k], 1);
                    }
                }
            }

            for (int i = 0; i < grid.GetLength(1); i++)
            {
                for (int j = 0; j < grid.GetLength(0); j++)
                {
                    if (isTransportTile(j, i))
                    {
                        
                        Vertex vertex = new Vertex(j, i);
                        List<Vertex> neighbors = neightborVerticesVertical(_vertices, vertex);
                        if (neighbors.Count == 2)
                        {
                            neighbors[0].addEdge(neighbors[1], 1);
                            neighbors[1].addEdge(neighbors[0], 1);


                        }
                        neighbors.Clear();
                        neighbors = neightborVerticesHorizontal(_vertices, vertex);
                        if (neighbors.Count == 2)
                        {
                            neighbors[0].addEdge(neighbors[1], 1);
                            neighbors[1].addEdge(neighbors[0], 1);


                        }




                    }
                }
            }

        }



        public void generatePlatform(int _width, int _height, MathProblem problem)
        {
            /* 2 : +
             * 3 : -
             * 20-40 : numbers
             */


            Debug.Log(problem.expression);
            Debug.Log(problem.solution);
            currentProblem = problem;
            string operations = problem.getOperationsFromExpression();
            string[] numbers = problem.getNumbersFromExpression();
            string numbersText = problem.getNumbersTextFromExpression();
            if (vertices != null)
            {
                vertices.Clear();
            }
            vertices = new List<Vertex>();
            grid = new int[_width, _height];
            for (int i = 0; i < grid.GetLength(1); i++)
            {
                for (int j = 0; j < grid.GetLength(0); j++)
                {
                    grid[j, i] = 1;
                }
            }

            for (int i = 0; i < grid.GetLength(1); i++)
            {
                for (int j = 0; j < grid.GetLength(0); j++)
                {
                    if (isNormalTile(j, i))
                    {
                        Vertex vertex = new Vertex(j, i, 1);

                        vertices.Add(vertex);
                    }
                    else if (isTransportTile(j, i))
                    {
                        Vertex vertex = new Vertex(j, i, 9);

                        vertices.Add(vertex);
                    }
                }
            }

            //grid[3, 3] = 9;
            //grid[3, 1] = 9;
            //grid[4, 2] = 9;
            for (int i = 0; i < operations.Length; i++)
            {
                if (operations[i] == '+')
                {
                    addRandomObject(2);
                }
                else if (operations[i] == '-')
                {
                    addRandomObject(3);
                }
            }

            for (int i = 0; i < numbers.Length; i++)
            {
                int number = int.Parse(numbers[i]);
                addRandomObject(20 + number);
            }
            addRandomObject(9);
            addRandomObject(9);
            addRandomBreakable();
            addRandomBreakable();
            addRandomBreakable();
            //addRandomObject(9);
            prepareEdges(vertices);




           
            int shortestStep = int.MaxValue;
            string bestExp = currentProblem.expression;
            List<Vertex> shortestPath = new List<Vertex>();
            List<string> betterSolutions = MathController.Instance.generateBetterSolutions(numbersText, operations, currentProblem.solution);
            for (int i = 0; i < betterSolutions.Count; i++)
            {
                MathProblem _problem = new MathProblem(betterSolutions[i], ExpressionEvaluator.Evaluate<int>(betterSolutions[i]));
                string _operations = _problem.getOperationsFromExpression();
                string[] _numbers = _problem.getNumbersFromExpression();
                List<string> allExp = new List<string>();
                int sayac = 0;
                for (int k = 0; k < _numbers.Length; k++)
                {
                    allExp.Add(_numbers[k]);
                    if (k < _numbers.Length - 1)
                    {
                        allExp.Add(_operations[sayac++].ToString());
                    }

                }
               
                //for (int j  = 0; j < allExp.Count; j++)
                //{
                //    Debug.Log("a : " + allExp[j]);
                //}
                List<Vertex> shortPath = simulateShortestPath(allExp);
                if(shortestStep > shortPath.Count)
                {
                    shortestPath = shortPath;
                    shortestStep = shortPath.Count;
                    bestExp = _problem.expression;
                }

            }

            Debug.Log("Best Expression : " + bestExp);
            Debug.Log("Shortest Step : " + shortestStep);
            //for (int i = 0; i < shortestPath.Count; i++)
            //{
            //    Debug.Log(shortestPath[i]);
            //}


            PlayerSimulator.Instance.calculateTravel(new Vertex(_width / 2, _height / 2, 1), shortestPath);

            NumberAndOperationAssets.Instance.setPathWithLines(shortestPath);
        }

        List<Vertex> cloneList(List<Vertex> list)
        {
            List<Vertex> result = new List<Vertex>();
            for (int i = 0; i < list.Count; i++)
            {
                result.Add(list[i].CloneOnlyVertex());
            }
            prepareEdges(result);

            return result;
        }

        List<Vertex> simulateShortestPath(List<string> allExp)
        {
            List<Vertex> finalPath = new List<Vertex>();
            List<Vertex> _vertices = cloneList(vertices);
            Vertex start = _vertices.First(s => isCenterOfPlatform(s.x, s.y));
            for (int i = 0; i < allExp.Count; i++)
            {
                int targetId = -1;
                if (allExp[i] == "+")
                {
                    targetId = 2;
                }
                else if (allExp[i] == "-")
                {
                    targetId = 3;
                }
                else
                {
                    targetId = int.Parse(allExp[i]) + 20;
                }
                //_vertices = _vertices.Where(s => s.type == 1 ||s.type == 8 || s.type == 9||s.type == targetId).ToList();
                List<Vertex> path = findNearestToPoint(_vertices, start, targetId);
                path.RemoveAt(0);

                finalPath.AddRange(path);
                start = _vertices.First(s => s == path[path.Count - 1]);
                start.type = 1;
            }
            return finalPath;

        }

        List<Vertex> findNearestToPoint(List<Vertex> _vertices, Vertex start, int id)
        {
            start:
            List<Vertex> sameTypes = _vertices.Where(s => s.type == id).ToList();
            int moveCount = int.MaxValue;
            List<Vertex> bestPath = null;

            for (int i = 0; i < sameTypes.Count; i++)
            {


                List<Vertex> path = GetShortestPath(start.x, start.y, sameTypes[i].x, sameTypes[i].y, _vertices, id);
                if (path.Count < moveCount)
                {
                    moveCount = path.Count;
                    bestPath = path;
                }
                else if (path.Count == moveCount)
                {
                    changeObjectPlaceInVertices(_vertices, sameTypes[i]);

                    goto start;
                }

            }
            return bestPath;
        }

        void changeObjectPlaceInVertices(List<Vertex> _vertices, Vertex vertex)
        {
            List<Vertex> neighbors = neightborVerticesRaw(_vertices, vertex);
            for (int i = 0; i < neighbors.Count; i++)
            {
                if(hasEmptyNeighbor(_vertices, neighbors[i]))
                {
                    Debug.Log("girdi");
                    int tempType = neighbors[i].type;
                    grid[neighbors[i].x, neighbors[i].y] = vertex.type;
                    grid[vertex.x, vertex.y] = tempType;

                    neighbors[i].type = vertex.type;
                    vertex.type = tempType;
                    return;
                }
            }

        }

        void createObjectsOnMap()
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                if(vertices[i].type == 2)
                {
                    NumberAndOperationAssets.Instance.createOperation('+', new Vector3(vertices[i].x, vertices[i].y));
                }
                else if (vertices[i].type == 3)
                {
                    NumberAndOperationAssets.Instance.createOperation('-', new Vector3(vertices[i].x, vertices[i].y));
                }
                else if (vertices[i].type == 9)
                {
                    NumberAndOperationAssets.Instance.createWall(new Vector3(vertices[i].x, vertices[i].y));
                }
                else if (vertices[i].type >= 20 && vertices[i].type <= 40)
                {
                    NumberAndOperationAssets.Instance.createNumber(vertices[i].type - 20, new Vector3(vertices[i].x, vertices[i].y));
                }
                else if (vertices[i].type == 70)
                {
                    NumberAndOperationAssets.Instance.createBreakable(new Vector3(vertices[i].x, vertices[i].y));
                }
            }
        }

        void addRandomObject(int id)
        {
            List<Vertex> emptyVertices = new List<Vertex>();
            for (int i = 0; i < vertices.Count; i++)
            {
                if(vertices[i].type == 1 && hasEmptyNeighbor(vertices, vertices[i], 3) && !isCenterOfPlatform(vertices[i].x, vertices[i].y))
                {
                    emptyVertices.Add(vertices[i]);
                }
            }
            int index = Random.Range(0, emptyVertices.Count);
            emptyVertices[index].type = id;
            if(id == 9)
            {
                grid[emptyVertices[index].x, emptyVertices[index].y] = id;
            }

        }

        void addRandomBreakable()
        {
            List<Vertex> emptyVertices = new List<Vertex>();
            for (int i = 0; i < vertices.Count; i++)
            {
                if (vertices[i].type == 1 && !hasTransportNeighbor(vertices, vertices[i]) && !isCenterOfPlatform(vertices[i].x, vertices[i].y))
                {
                    emptyVertices.Add(vertices[i]);
                }
            }
            if(emptyVertices.Count> 0)
            {
                int index = Random.Range(0, emptyVertices.Count);
                emptyVertices[index].type = 70;
            }

        }

        bool isCenterOfPlatform(int x, int y)
        {
            return x == grid.GetLength(0) / 2 && y == grid.GetLength(1) / 2;
        }

        bool hasEmptyNeighbor(List<Vertex> _vertices, Vertex v, int neighborCount = 3)
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                List<Vertex> neighbors = neightborVerticesRaw(_vertices, _vertices[i]);
                if(neighbors.Count >= neighborCount)
                {

                    return true;
                }

            }
            return false;
        }

        bool hasTransportNeighbor(List<Vertex> _vertices, Vertex v)
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                List<Vertex> neighbors = neightborVerticesRaw(_vertices, v);
                for (int j = 0; j < neighbors.Count; j++)
                {
                    if(neighbors[j].type == 9)
                    {
                        return true;
                    }
                }

            }
            return false;
        }

        List<Vertex> neightborVerticesRaw(List<Vertex> _vertices, Vertex vertex)
        {
            List<Vertex> result = new List<Vertex>();
            if(isValidPos(vertex.x + 1, vertex.y))
            {
                result.Add(_vertices.First(s => s.x == vertex.x + 1 && s.y == vertex.y));
            }
            if (isValidPos(vertex.x - 1, vertex.y))
            {
                result.Add(_vertices.First(s => s.x == vertex.x - 1 && s.y == vertex.y));
            }
            if (isValidPos(vertex.x, vertex.y + 1))
            {
                result.Add(_vertices.First(s => s.x == vertex.x && s.y == vertex.y + 1));
            }
            if (isValidPos(vertex.x, vertex.y - 1))
            {
                result.Add(_vertices.First(s => s.x == vertex.x && s.y == vertex.y - 1));
            }
            return result;
        }

        List<Vertex> neightborVerticesHorizontal(List<Vertex> _vertices, Vertex vertex)
        {
            List<Vertex> result = new List<Vertex>();
            if (isValidPos(vertex.x + 1, vertex.y))
            {
                result.Add(_vertices.First(s => s.x == vertex.x + 1 && s.y == vertex.y));
            }
            if (isValidPos(vertex.x - 1, vertex.y))
            {
                result.Add(_vertices.First(s => s.x == vertex.x - 1 && s.y == vertex.y));
            }

            return result;
        }

        List<Vertex> neightborVerticesVertical(List<Vertex> _vertices, Vertex vertex)
        {
            List<Vertex> result = new List<Vertex>();

            if (isValidPos(vertex.x, vertex.y + 1))
            {
                result.Add(_vertices.First(s => s.x == vertex.x && s.y == vertex.y + 1));
            }
            if (isValidPos(vertex.x, vertex.y - 1))
            {
                result.Add(_vertices.First(s => s.x == vertex.x && s.y == vertex.y - 1));
            }
            return result;
        }

        bool isNormalTile(int x, int y)
        {
            return isValidPos(x, y) && grid[x, y] == 1;
        }

        bool isTransportTile(int x, int y)
        {
            return isValidPos(x, y) && grid[x, y] == 9;
        }

        bool isValidPos(int x, int y)
        {
            return x >= 0 && x < grid.GetLength(0) && y >= 0 && y < grid.GetLength(1);
        }

        List<Vertex> GetShortestPath(int x1, int y1, int x2, int y2, List<Vertex> _vertices, int targetId)
        {
            Vertex start = _vertices.First(s => s.x == x1 && s.y == y1);
            Vertex end = _vertices.First(s => s.x == x2 && s.y == y2);
            List<Vertex> path = new List<Vertex>();
            List<Vertex> unvisited = new List<Vertex>();
            Dictionary<Vertex, Vertex> previous = new Dictionary<Vertex, Vertex>();

            // The calculated distances, set all to Infinity at start, except the start Node
            Dictionary<Vertex, float> distances = new Dictionary<Vertex, float>();

            for (int i = 0; i < _vertices.Count; i++)
            {
                Vertex node = _vertices[i];
                unvisited.Add(node);

                // Setting the node distance to Infinity
                distances.Add(node, float.MaxValue);
            }
            distances[start] = 0f;
            while (unvisited.Count != 0)
            {
                unvisited = unvisited.OrderBy(node => distances[node]).ToList();
                Vertex current = unvisited[0];
                //Debug.Log(current.ToString() + " / " + end.ToString());
                // Remove the current node from unvisisted list
                unvisited.Remove(current);

                if (current.x == end.x && current.y == end.y)
                {
                    
                    // Construct the shortest path
                    while (previous.ContainsKey(current))
                    {
                        
                        // Insert the node onto the final result
                        path.Insert(0, current);

                        // Traverse from start to end
                        current = previous[current];
                    }

                    // Insert the source onto the final result
                    path.Insert(0, current);

                    return path;
                }

                // Looping through the Node connections (neighbors) and where the connection (neighbor) is available at unvisited list
                for (int i = 0; i < current.edges.Count; i++)
                {
                    Vertex neighbor = current.edges[i].Vertex2;
                    if(neighbor.type != 1 && neighbor.type != targetId && neighbor.type != 70)
                    {
                        continue;
                    }

                    // Getting the distance between the current node and the connection (neighbor)
                    float length = current.edges[i].Distance;

                    // The distance from start node to this connection (neighbor) of current node
                    float alt = distances[current] + length;

                    // A shorter path to the connection (neighbor) has been found
                    if (alt < distances[neighbor])
                    {
                        distances[neighbor] = alt;
                        previous[neighbor] = current;
                    }
                }
            }

            return null;


        }
    }

    public class Vertex
    {



        public int x; // Horizontal location on screen

        public int y; // Vertical location on screen

        public int type;

        public List<Edge> edges;
        // Construcutor

        public Vertex(int x, int y, int t = 1)

        {

            type = t;
            this.x = x;

            this.y = y;

            edges = new List<Edge>();
            


        }

        public void addEdge(Vertex vertex2, float distance)
        {
            edges.Add(new Edge(this, vertex2, distance));
        }

        public override string ToString()

        {

            return (x.ToString() + " – " + y.ToString());

        }

        public Vertex CloneOnlyVertex()
        {
            return new Vertex(this.x, this.y, this.type);
        }

    }

    public class Edge

    {

        public Vertex Vertex1; // Vertex one

        public Vertex Vertex2; // Vertex two

        public float Distance; // DIstance or similar

        public float Cost; // Cost or multiplier factor

        // Contructor

        public Edge(Vertex Vertex1, Vertex Vertex2, float distance)

        {

            this.Vertex1 = Vertex1;

            this.Vertex2 = Vertex2;

            this.Distance = distance;


        }



    }

}


