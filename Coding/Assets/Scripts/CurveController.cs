﻿using System.Collections;
using System.Collections.Generic;
using BansheeGz.BGSpline.Curve;
using BGSplitterPolyline = BansheeGz.BGSpline.Components.BGCcSplitterPolyline;
using UnityEngine;

public class CurveController : MonoBehaviour 
{
    public BGCurve goldCurve;

    public static CurveController Instance;

    private void Awake()
    {
        Instance = this;
    }
    // Use this for initialization
    void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public Vector3[] getPointsFromGoldCurve(Vector3 startPos, Vector3 endPos)
    {
        goldCurve.Points[0].PositionWorld = startPos;
        BGSplitterPolyline splittedLines = goldCurve.GetComponent<BGSplitterPolyline>();
        return Utility.ListToArray(splittedLines.Positions);
    }
}
