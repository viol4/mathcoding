﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;

public class MathController : MonoBehaviour 
{

    private const int maxNumber = 10;

    public List<string> allCombs;
    public List<string> allOperationCombs;
    List<string> allExpressions;
    Recursion rec;
    float time = 0f;

    public static MathController Instance;

    private void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start () 
    {

        string easyProb = generateLongEasy(2);

        initAI();
        //for (int i = 0; i < 100; i++)
        //{

        //    string exp = generateLongHard(3);
        //    MathProblem problem = new MathProblem(exp, ExpressionEvaluator.Evaluate<int>(exp));
        //    fixProblem(problem);
        //    Debug.Log(problem.expression + " = " + problem.solution);
        //}

        //f("5252", 5);

        //List<string> r = findOperationPerm("+--");
        //for (int i = 0; i < r.Count; i++)
        //{
        //    Debug.Log(r[i]);
        //}



        //for (int i = 1; i < arr.Length + 1; i++)
        //{
        //    printCombination(arr, arr.Length, i, allCombs);
        //}

        //for (int i = 0; i < allCombs.Count; i++)
        //{
        //    Debug.Log(allCombs[i]);
        //}



    }


    public MathProblem generateMathProblemEasy(int count = 2)
    {
        //string exp = generateLongEasy(count);
        string exp = "8+8+8+8";
        return new MathProblem(exp, ExpressionEvaluator.Evaluate<int>(exp));
    }

    public void initAI()
    {
        time = Time.realtimeSinceStartup;
        allCombs = new List<string>();
        allOperationCombs = new List<string>();
        allExpressions = new List<string>();
        rec = new Recursion();

    }



    public List<string> generateBetterSolutions(string numbers, string operations, int solution)
    {
        allExpressions.Clear();
        generateAllExpressions(numbers, operations, solution);
        return allExpressions;
    }

    void generateAllExpressions(string numbers, string operations, int solution)
    {
        
        char[] numberArr = numbers.ToCharArray();
        char[] opArr = operations.ToCharArray();
        allCombs.Clear();
        allOperationCombs.Clear();

        for (int i = 1; i < numberArr.Length + 1; i++)
        {
            printCombination(numberArr, numberArr.Length, i, allCombs);
        }

        for (int i = 1; i < opArr.Length + 1; i++)
        {
            printCombination(opArr, opArr.Length, i, allOperationCombs);
        }

        for (int i = 0; i < allCombs.Count; i++)
        {
            if(allCombs[i].Length > 1)
            {
                for (int j = 0; j < allOperationCombs.Count; j++)
                {
                    if(allOperationCombs[j].Length == allCombs[i].Length - 1)
                    {
                        string exp = allCombs[i];
                        for (int k = 0; k < allOperationCombs[j].Length; k++)
                        {
                            exp = exp.Insert(k * 2 + 1, allOperationCombs[j][k].ToString());
                        }
                        if(ExpressionEvaluator.Evaluate<int>(exp) == solution)
                        {
                            allExpressions.Add(exp);
                            if(allExpressions.Count == 10)
                            {
                                return;
                            }
                        }


                    }
                }
            }
        }


    }
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    void findPermutations(string digits)
    {
        for (int i = 0; i < Mathf.Pow(2, digits.Length); i++)
        {
            List<char> chars = new List<char>();
            for (int j = 0; j < digits.Length; j++)
            {
                if ((i & (1 << (digits.Length - j - 1))) != 0)
                {
                    chars.Add(digits[j]);
                }
            }
            //Debug.Log(combination[0] + combination[1] + combination[2] + combination[3] + combination[4]);
        }
    }

    //int findResult(List<char> chars, int result)
    //{
        
    //}

    List<string> findOperationPerm(string operations)
    {
        List<string> result = new List<string>();
        for (int i = 0; i < Mathf.Pow(2, operations.Length); i++)
        {
            string chars = "";
            for (int j = 0; j < operations.Length; j++)
            {
                if ((i & (1 << (operations.Length - j - 1))) != 0)
                {
                    chars += operations[j];
                }
            }
            result.Add(chars);
        }
        result.RemoveAt(0);
        return result;
    }

    void printCombination(char[] arr, int n, int r, List<string> combs)
    {
        // A temporary array to store all combination one by one
        char[] data = new char[r];

        // Print all combination using temprary array 'data[]'
        combinationUtil(arr, data, 0, n - 1, 0, r, combs);
    }

    /* arr[]  ---> Input Array
       data[] ---> Temporary array to store current combination
       start & end ---> Staring and Ending indexes in arr[]
       index  ---> Current index in data[]
       r ---> Size of a combination to be printed */
    void combinationUtil(char[] arr, char[] data, int start, int end,
                         int index, int r,  List<string> combs)
    {
        // Current combination is ready to be printed, print it
        if (index == r)
        {
            string output = "";
            for (int j = 0; j < r; j++)
                output += data[j];
            if(!combs.Contains(output))
            {
                combs.Add(output);
            }

            rec.InputSet = rec.MakeCharArray(output);
            rec.CalcPermutation(0, combs);
            return;
        }

        // replace index with all possible elements. The condition
        // "end-i+1 >= r-index" makes sure that including one element
        // at index will make a combination with remaining elements
        // at remaining positions
        for (int i = start; i <= end && end - i + 1 >= r - index; i++)
        {
            data[index] = arr[i];
            combinationUtil(arr, data, i + 1, end, index + 1, r, combs);
        }
    }

    public void fixProblem(MathProblem problem)
    {
        int counter = 0;
        while(problem.solution < 0)
        {
            for (int i = 0; i < problem.expression.Length; i++)
            {
                if(problem.expression[i] == '-')
                {
                    problem.expression = problem.expression.Remove(i, 1);
                    problem.expression = problem.expression.Insert(i, "+");
                    problem.solution = ExpressionEvaluator.Evaluate<int>(problem.expression);
                    return;
                }
            }
            counter++;
            if (counter > 100)
                return;
        }
    }

    string generateSubtractionEasy()
    {
        int number1 = Random.Range(0, maxNumber);
        int number2 = Random.Range(0, number1 + 1);
        return number1 + "-" + number2;
    }

    string generateAdditionEasy()
    {
        int number1 = Random.Range(0, maxNumber);
        int number2 = Random.Range(0, maxNumber);
        return number1 + "+" + number2;
    }


    string generateDivision(int max = maxNumber)
    {
        int number1 = Random.Range(1, max);
        List<int> divisors = allDivisors(number1);
        int number2 = divisors[Random.Range(0, divisors.Count)];
        return number1 + "/" + number2;
    }
    string generateMultiplication()
    {
        int number1 = Random.Range(0, maxNumber);
        int number2 = Random.Range(0, maxNumber);
        return number1 + "*" + number2;
    }

    string generateLongEasy(int count)
    {
        string exp = "";
        int number1 = Random.Range(5, maxNumber);
        exp += number1;
        int lastNumber = number1;
        for (int i = 0; i < count-1; i++)
        {
     
            if(Random.value < 0.5f && lastNumber != 0)
            {
                exp += "-";
                int number2 = Random.Range(0, lastNumber);
                exp += number2;
                lastNumber = number2;

            }
            else
            {
                exp += "+";
                int min = 0;
                if(lastNumber == 0)
                {
                    min = Random.Range(3, 6);
                }
                int number2 = Random.Range(min, maxNumber);
                exp += number2;
                lastNumber = number2;
            }
        }
        return exp;
    }

    string generateLongHard(int count)
    {
        string exp = "";
        //int number1 = Random.Range(5, maxNumber);
        float rnd1 = Random.value;
        int lastNumber = 0;
        int totalCount = 0;
        if(rnd1 < 0.33f)
        {
            exp += generateMultiplication();
            lastNumber = int.Parse(exp[exp.Length - 1].ToString());
            totalCount += 2;
        }
        else if (rnd1 < 0.66f)
        {
            exp += generateDivision();
            lastNumber = int.Parse(exp[exp.Length - 1].ToString());
            totalCount += 2;
        }
        else
        {
            int a = Random.Range(0, maxNumber);
            exp += a;
            lastNumber = a;
            totalCount += 1;
        }

        string lastOperation = "";

        int lastSolution = lastNumber;
        for (; totalCount < count; )
        {
            float rnd = Random.value;

            if(rnd < 0.25f && count - totalCount > 1)
            {
                exp += "+";
                string multiplication = generateMultiplication();
                lastOperation = "*";
                lastNumber = int.Parse(multiplication[multiplication.Length - 1].ToString());
                exp += multiplication;
                totalCount += 2;
                lastSolution = ExpressionEvaluator.Evaluate<int>(exp);
            }
            else if (rnd < 0.5f && lastOperation != "/" && count - totalCount > 1)
            {
                string division = "";
                if(Random.value < 0.5f && lastSolution != 0)
                {
                    exp += "-";
                    division = generateDivision(lastNumber);
                }
                else
                {
                    exp += "+";
                    division = generateDivision();
                }


                lastOperation = "/";
                lastNumber = int.Parse(division[division.Length - 1].ToString());
                exp += division;
                totalCount += 2;
                lastSolution = ExpressionEvaluator.Evaluate<int>(exp);
            }
            else if (rnd < 0.75f && lastNumber != 0)
            {
                exp += "-";
                lastOperation = "-";
                int number2 = Random.Range(0, lastNumber);
                exp += number2;
                lastNumber = number2;
                totalCount += 1;
                lastSolution = ExpressionEvaluator.Evaluate<int>(exp);
            }
            else
            {
                exp += "+";
                lastOperation = "+";
                int min = 0;
                if (lastNumber == 0)
                {
                    min = Random.Range(3, 6);
                }
                int number2 = Random.Range(min, maxNumber);
                exp += number2;
                lastNumber = number2;
                totalCount += 1;
                lastSolution = ExpressionEvaluator.Evaluate<int>(exp);
            }

        }
        return exp;
    }





    List<int> allDivisors(int number)
    {
        List<int> divisors = new List<int>();   
        for (int i = number; i >= 1; i--)
        {
            if(number % i == 0)
            {
                divisors.Add(i);
            }
        }
        return divisors;
    }

    void check(int sum, int previous, string digits, int target, string expr) 
    {
        if (digits.Length == 0) 
        {
            if (sum + previous == target) 
            {
                Debug.Log(expr + " = " + target);
            }
        } 
        else 
        {
            for (int i = 1; i <= digits.Length; i++) 
            {
                int current = int.Parse(digits.Substring(0, i));
                string remaining = digits.Substring(i);
                //check(sum + previous, current, remaining, target, expr + " + " + current);
                ////check(sum, previous * current, remaining, target, expr + " * " + current);
                check(sum, previous / current, remaining, target, expr + " / " + current);
                //check(sum + previous, -current, remaining, target, expr + " - " + current);
            }
        }
    }

    void f(string digits, int target) 
    {
        for (int i = 1; i <= digits.Length; i++)
        {
            string current = digits.Substring(0, i);
            check(0, int.Parse(current), digits.Substring(i), target, current);
        }
    } 


}

public class MathProblem
{
    public string expression
    {
        get;
        set;
    }

    public int solution
    {
        get;
        set;
    }


    public MathProblem(string exp, int sol)
    {
        expression = exp;
        solution = sol;
    }

    public string getOperationsFromExpression()
    {
        string ops = "";
        for (int i = 0; i < expression.Length; i++)
        {
            if (isOperation(expression[i]))
            {
                ops += expression[i];
            }
        }
        return ops;
    }

    public string[] getNumbersFromExpression()
    {
        return expression.Split(new char[] { '+', '-', '/', '*' });
    }

    public string getNumbersTextFromExpression()
    {
        string[] array = getNumbersFromExpression();
        string result = "";
        for (int i = 0; i < array.Length; i++)
        {
            result += array[i];
        }
        return result;
    }
    bool isOperation(char c)
    {
        return c == '+' || c == '-' || c == '/' || c == '*';
    }
}

class Recursion
{
    private int elementLevel = -1;
    private int numberOfElements;
    private int[] permutationValue = new int[0];

    private char[] inputSet;
    public char[] InputSet
    {
        get { return inputSet; }
        set { inputSet = value; }
    }

    private int permutationCount = 0;
    public int PermutationCount
    {
        get { return permutationCount; }
        set { permutationCount = value; }
    }

    public char[] MakeCharArray(string InputString)
    {
        char[] charString = InputString.ToCharArray();
        permutationValue = new int[charString.Length];

        numberOfElements = charString.Length;
        return charString;
    }

    public void CalcPermutation(int k, List<string> comb)
    {
        elementLevel++;
        permutationValue.SetValue(elementLevel, k);

        if (elementLevel == numberOfElements)
        {
            OutputPermutation(permutationValue, comb);
        }
        else
        {
            for (int i = 0; i < numberOfElements; i++)
            {
                if (permutationValue[i] == 0)
                {
                    CalcPermutation(i, comb);
                }
            }
        }
        elementLevel--;
        permutationValue.SetValue(0, k);
    }

    private void OutputPermutation(int[] value, List<string> combs)
    {
        string output = "";
        foreach (int i in value)
        {
            output += inputSet.GetValue(i - 1);
        }
        if(!combs.Contains(output))
        {
            combs.Add(output);
        }
        PermutationCount++;
    }
}


