﻿using System.Collections;
using System.Collections.Generic;
using AlisCoding;
using UnityEngine;

public class NumberAndOperationAssets : MonoBehaviour
{
    public Sprite[] numberSprites;

    public GameObject numberPrefab;
    public GameObject additionPrefab;
    public GameObject subtractionPrefab;
    public GameObject wallPrefab;
    public GameObject breakablePrefab;
    public Transform tileMap;
    public LineRenderer lineRenderer;

    public static NumberAndOperationAssets Instance;

    private void Awake()
    {
        Instance = this;
    }
    // Use this for initialization
    void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public void createNumber(int number, Vector3 pos)
    {
        GameObject numberGO = Instantiate(numberPrefab);
        numberGO.GetComponent<SpriteRenderer>().sprite = numberSprites[number];
        numberGO.transform.position = tileMap.position + pos;
    }

    public void createWall(Vector3 pos)
    {
        GameObject wallGO = Instantiate(wallPrefab);
        wallGO.transform.position = tileMap.position + pos;
    }
    public void createBreakable(Vector3 pos)
    {
        GameObject breakableGO = Instantiate(breakablePrefab);
        breakableGO.transform.position = tileMap.position + pos;
    }

    public void createOperation(char op, Vector3 pos)
    {
        GameObject operationGO = null;
        if(op == '+')
        {
            operationGO = Instantiate(additionPrefab);
        }
        else if (op == '-')
        {
            operationGO = Instantiate(subtractionPrefab);
        }
        operationGO.transform.position = tileMap.position + pos;
    }

    public void setPathWithLines(List<Vertex> bestPath)
    {
        lineRenderer.positionCount = bestPath.Count + 1;
        lineRenderer.SetPosition(0, PlayerController.Instance.transform.position);
        for (int i = 0; i < bestPath.Count; i++)
        {
            lineRenderer.SetPosition(i+1, tileMap.position + new Vector3(bestPath[i].x, bestPath[i].y));
        }
    }


}
