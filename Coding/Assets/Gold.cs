﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gold : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(GameController.Instance.canCollectGoldFromGround() && collision.tag == "_Player")
        {
            GameController.Instance.collectedGoldGoesToUI(transform);
        }
    }
}
